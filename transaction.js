function initDB() {
    const animalsArray = [
         {"type":"Lizard","price":10.50, "purchases": [], 'stock': 10},
         {"type":"Dog","price":20.65, "purchases": [], 'stock': 8},
         {"type":"hamster","price":5.00, "purchases": [], 'stock': 15},
         {"type":"parrot","price":25.00, "purchases": [], 'stock': 1000},
         {"type":"horse","price":99.99, "purchases": [], 'stock': 3},

    ];     
   
    const users = [
        {"gender":"Female","first_name":"Joela","email":"jbromet0@e-recht24.de",
        "address":[{"street":"870 Menomonie Park","country":"Indonesia","state":null}]},
        {"gender":"Male","first_name":"Boonie","email":"bcoal1@psu.edu",
        "address":[{"street":"911 Manitowish Court","country":"Russia","state":null}]},
        {"gender":"Female","first_name":"Nonnah","email":"njakobsson2@gnu.org",
        "address":[{"street":"58 Fisk Place","country":"Russia","state":null}]},
        {"gender":"Female","first_name":"Bonnibelle","email":"bmcgonagle3@amazon.com",
        "address":[{"street":"5787 Schiller Center","country":"China","state":null}]},
        {"gender":"Male","first_name":"Patsy","email":"pbourley4@indiegogo.com",
        "address":[{"street":"7 Stang Terrace","country":"Brazil","state":null}]},
        {"gender":"Male","first_name":"Sol","email":"saynsley5@globo.com",
        "address":[{"street":"08949 Loomis Way","country":"Kazakhstan","state":null}]},
        {"gender":"Male","first_name":"Sarge","email":"stortoishell6@utexas.edu",
        "address":[{"street":"8 Golf Way","country":"South Africa","state":null}]},
        {"gender":"Male","first_name":"Dill","email":"dgauntlett7@chronoengine.com",
        "address":[{"street":"1627 Clemons Pass","country":"Costa Rica","state":null}]}
    ]

    let stats = db.animals.stats();
    if("errmsg" in stats){
        print("Creating animal entries");
        db.animals.insertMany(animalsArray);
        print("Animals collection size: " + db.animals.stats().count);
    } else{
        print("Animals collection size: " + db.animals.stats().count);
    }

    stats = db.customers.stats();
    if("errmsg" in stats){
        print("Creating customer entries");
        db.customers.insertMany(users);
        print("Customers collection size: " + db.animals.stats().count);
    } else{
        print("Customers collection size: " + db.animals.stats().count);
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function purchasePet(rA, rC) {
    //  start the session and define the collections to use
    session = db.getMongo('rs0/localhost:6667').startSession();
    db = session.getDatabase("petshop");
    animals = session.getDatabase("petshop").animals;
    customers = session.getDatabase("petshop").customers;
    print('Starting transaction')

    //  start a transaction
    session.startTransaction( { readConcern: { level: "snapshot" }, writeConcern: { w: "majority" } } );

    //  gets the second entry of each collection
    c = customers.find().skip(rC).limit(1).toArray()[0];
    a = animals.find().skip(rA).limit(1).toArray()[0];

    //  Update the amount spent
    if ('spent' in c) {
        c.spent += a.price;
    } else {
        c.spent = a.price;
    }
    customers.update({'_id': c._id}, c);

    //  Insert the purchase inside the animal document and reduce its stock by one
    a.purchases.push({'buyer': { '_id': c._id, 'first_name': c.first_name }, 'purchase_time': new Date()});
    a.stock = a.stock - 1;
    animals.update({'_id': a._id}, a);

    // retrieve the animal record again
    const a_test = animals.findOne({'_id': a._id})
    // If the stock is less than zero we abort else it's ok to commit
    if (a_test.stock < 0){
        session.abortTransaction();
        print('Aborted transaction of: ' +c.first_name  + ' for purchase because stock of ' + a.type + ' is 0')
    } else {
        session.commitTransaction();
        print('Customer ' + c.first_name + ' succesfuly bought a ' + a.type + ' total amount spent:' + c.spent)
    }


}

db = db.getSiblingDB('petshop');
initDB();

while (true) {
    print("Sleeping 1 sec");
    sleep(1000);
    r1 = getRandomInt(0,4);
    r2 = getRandomInt(0,7);
    purchasePet(r1, r2);


}
